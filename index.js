const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var cors = require('cors');
const port = process.env.PORT || 3800;
var soap = require('strong-soap').soap;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
// var soap = require('soap');






app.post('/pruebaSoap', async function(req, res) {

    var url = req.body.endPoint;
    var requestArgs = req.body.args;
    var metodo = req.body.method;

    try {
        var options = {};
        soap.createClient(url, options, function(err, client) {
            var method = client[metodo];
            method(requestArgs, function(err, result, envelope, soapHeader) {
                tablas = {
                    tables: result.return
                };
                res.json(tablas);
            });
        });
    } catch (e) {
        res.json({});
        throw e;
    }

});




// app.get('holaMundo', async function(req, res) {
//     res.json({ mensaje: 'hola' });
// });

app.listen(port, function() {
    console.log('El sitio de APIs inició correctamente en el puerto: ', port);
});